SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


DROP TABLE IF EXISTS `comentarios`;
CREATE TABLE IF NOT EXISTS `comentarios` (
  `ID` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `usId` int(20) unsigned NOT NULL,
  `imgId` int(20) unsigned NOT NULL,
  `texto` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `usId` (`usId`),
  KEY `imgId` (`imgId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

DROP TABLE IF EXISTS `imagenes`;
CREATE TABLE IF NOT EXISTS `imagenes` (
  `ID` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `usId` int(8) unsigned NOT NULL,
  `nombre` varchar(25) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `fecha` varchar(100) DEFAULT NULL,
  `temaId` int(8) unsigned NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `usId` (`usId`),
  KEY `temaId` (`temaId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

DROP TABLE IF EXISTS `temas`;
CREATE TABLE IF NOT EXISTS `temas` (
  `ID` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `tema` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `tema` (`tema`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

INSERT INTO `temas` (`ID`, `tema`) VALUES
(1, 'Animales'),
(2, 'Arte'),
(3, 'Deportes'),
(4, 'Naturaleza'),
(5, 'Sin categoria'),
(6, 'Tecnologia');

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE IF NOT EXISTS `usuarios` (
  `ID` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(25) DEFAULT NULL,
  `premium` char(10) NOT NULL DEFAULT 'no',
  `email` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

INSERT INTO `usuarios` (`ID`, `username`, `password`, `premium`, `email`) VALUES
(1, 'admin', 'admin', 'si', 'admin@test.es'),
(2, 'usuario1', 'usuario1', 'si', 'usuario1@test.com'),
(3, 'usuario2', 'usuario2', 'no', 'usuario2@test.com'),
(4, 'usuario3', 'usuario3', 'no', 'usuario3@test.com');


ALTER TABLE `comentarios`
  ADD CONSTRAINT `comentarios_ibfk_2` FOREIGN KEY (`imgId`) REFERENCES `imagenes` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `comentarios_ibfk_1` FOREIGN KEY (`usId`) REFERENCES `usuarios` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `imagenes`
  ADD CONSTRAINT `imagenes_ibfk_2` FOREIGN KEY (`temaId`) REFERENCES `temas` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `imagenes_ibfk_1` FOREIGN KEY (`usId`) REFERENCES `usuarios` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
