<?php

class Controller {
    public function inicio() {
        $tema = "";
        $params = array();
        $m = new Model(Config::$mvc_bd_nombre, Config::$mvc_bd_usuario,Config::$mvc_bd_clave, Config::$mvc_bd_hostname);
        $params2 = array(
            'temas'  => $m->DameTemas()
        );

            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                 $tema = $_POST['tema'];

                if($tema != "Todas") {
                    if($m->buscarPorTema($tema) !=false) {
                       $params = array(
                            'imagenes'  => $m->buscarPorTema($tema) 
                        );
                    }
                } else {
                    if($m->dameImagenes() != false){
                        $params = array(
                            'imagenes'  => $m->dameImagenes() 
                        );
                    }
                }
            } else if($m->dameImagenes() != false){
                 $params = array(
                    'imagenes'  => $m->dameImagenes() 
                );
            }

        require __DIR__ . '/templates/inicio.php';
    }  

    public function registro() {

        $params = array(
            'username' => '',
            'password' => '',
            'email' => ''
        );

        $m = new Model(Config::$mvc_bd_nombre, Config::$mvc_bd_usuario, Config::$mvc_bd_clave, Config::$mvc_bd_hostname);

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            // comprobar campos formulario

            if(isset($_POST['username']) && isset($_POST['password']) && isset($_POST['password2']) 
                && isset($_POST['email']) && isset($_POST['premium']) && $_POST['username'] != "" 
                && $_POST['password'] != ""  && $_POST['email'] != "") {

                if($m->validarDatos($_POST['username'], $_POST['password'], $_POST['password2'])) {
                    if($_POST['password2'] == $_POST['password']) {
                        $res= $m->insertarUsuario($_POST['username'], $_POST['password'], $_POST['email'], $_POST['premium']);
                        
                        if($res != false){
                             $params['mensaje'] = "usuario registrado";
                        } else {
                                $params['mensaje'] = "usuario repetido";
                        }
                    } else {
                        $params = array(
                            'username' => $_POST['username'],
                            'password' => '',
                            'email' => ''
                        );

                        $params['mensaje'] = 'No se ha podido insertar el usuario, comprueba que la contraseña es la misma';
                    }
                }
            } else {
                 $params['mensaje'] = 'Faltan campos por rellenar';
            }
        }

        require __DIR__ . '/templates/registro.php';
    }

     public function login() {
        $params = array(
            'username' => '',
            'password' => ''
        );

        $m = new Model(Config::$mvc_bd_nombre, Config::$mvc_bd_usuario,Config::$mvc_bd_clave, Config::$mvc_bd_hostname);

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            // comprobar campos formulario

            if($m->validarUsuario($_POST['username'], $_POST['password'])) {
              $usuario = array();
              $usuario = $m->loguearUsuario($_POST['username'], $_POST['password']);

                if(count($usuario) > 0) {
                    $_SESSION["s_username"] = $usuario[0]['username'];
                    $_SESSION["ID"] = $usuario[0]['ID'];
                    $_SESSION["email"] = $usuario[0]['email'];
                    $_SESSION["pass"] = $usuario[0]['password'];
                    $_SESSION["premium"] = $usuario[0]['premium'];
                    $_SESSION["logeado"] = "SI";
                    header('Location: index.php?ctl=inicio');

                } else {
                    $params['mensaje'] = "login incorrecto";
                }

            } else {
                $params = array(
                    'username' => '',
                    'password' => ''
                );

                $params['mensaje'] = 'No se ha podido iniciar sesión';
            }
        }
        require __DIR__ . '/templates/login.php';
    }

    public function verGalerias() {

        $m = new Model(Config::$mvc_bd_nombre, Config::$mvc_bd_usuario,Config::$mvc_bd_clave, Config::$mvc_bd_hostname);

        $params = array(
            'usuarios'  => $m->dameUsuariosConGaleria() 
        );

        require __DIR__ . '/templates/galerias.php';
    }

    public function verGaleria() {

        if (!isset($_GET['id'])) {
            throw new Exception('Página no encontrada');
        }
        
        //buscamos por id del usuario

        $userId = htmlentities($_GET['id']);

        $m = new Model(Config::$mvc_bd_nombre, Config::$mvc_bd_usuario,Config::$mvc_bd_clave, Config::$mvc_bd_hostname);

        $galeria = $m->verGaleria($userId);
        $params = array('galeria' => $galeria);
        
        require __DIR__ . '/templates/galeria.php';
    }

    public function verImagen() {

        if (!isset($_GET['id'])) {
            throw new Exception('Página no encontrada');
        }
        
        $id = $_GET['id'];

        $m = new Model(Config::$mvc_bd_nombre, Config::$mvc_bd_usuario,Config::$mvc_bd_clave, Config::$mvc_bd_hostname);

        $imagen = $m->verImagen($id);
        $comentarios = $m->dameComentarios($id);
        $params = array('imagen' => $imagen);
        $params2 = array('comentarios' => $comentarios);

        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            if(isset($_POST['comentario']) && $_POST['comentario'] != "") {
                $texto = $_POST['comentario'];
                $idUser = $_SESSION['ID'];
                $idImagen = $id;

                if($m->insertarComentario($idUser,$idImagen,$texto) != false) {
                    $params['mensaje'] = "Comentario añadido";
                    header('Location: index.php?ctl=ver&id='.$id);
                } else {
                    $params['mensaje'] = "No se ha podido añadir el comentario";
                }
            } else {
                $params['mensaje'] = "El comentario no puede estar vacio";
            }
        }

        require __DIR__ . '/templates/ver.php';
    }

    public function verPerfil() {

        $id = $_SESSION['ID'];
        $m = new Model(Config::$mvc_bd_nombre, Config::$mvc_bd_usuario,Config::$mvc_bd_clave, Config::$mvc_bd_hostname);
        $params = array(
            'imagenes' => $m->dameImagenesUser($id),
            'temas'  => $m->DameTemas() 
        );

        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            if(isset($_POST['passwordViejo']) && isset($_POST['password1'])) {
                if($_POST['passwordViejo'] == $_SESSION['pass']) {
                    if($_POST['password1'] == $_POST['password2'] && $_POST['password1'] =! "" && $_POST['password2'] != "" ) {

                        $password = htmlentities($_POST['password2']);
                        $id = $_SESSION['ID'];

                        $m = new Model(Config::$mvc_bd_nombre, Config::$mvc_bd_usuario,Config::$mvc_bd_clave, Config::$mvc_bd_hostname);

                        if($m -> cambiarPassword($id, $password)) {
                            $params['mensaje'] = "Contraseña cambiada.";
                            $_SESSION["pass"] = $password;
                            header('refresh:1; url=index.php?ctl=perfil');
                        } else {
                          $params['mensaje'] = "No se ha podido cambiar la contraeña.";
                        }
                    } else {
                        $params['mensaje'] = "El password nuevo no coincide en los dos campos o no se han rellenado";
                    }

                } else {
                    $params['mensaje'] = "El password de usuario introducido es incorrecto.";
                }
            }
            if(isset($_POST['idImagen'])) {
                $idImg = $_POST['idImagen'];
                $res = $m->borrarImg($idImg);
                $res= $m->borrarFoto($idImg);
                if($res != false) {
                    $params['mensaje'] = "Foto borrada";
                    header('refresh:1; url=index.php?ctl=perfil');
                } else {
                    $params['mensaje'] = "Foto no borrada";
                }
            }
            if(isset($_POST['nombre']) && isset($_POST['tema'])) {
                if($_POST['nombre'] != "" && $_FILES['imagen']['name'] != "") {
                    $nombreFichero = date('H-i-s')."-".$_FILES['imagen']['name'];
                    $id = $_SESSION['ID'];
                    $url = "fotos_user_$id/$nombreFichero";
                    $nombre = $_POST['nombre'];
                    $fecha = date("Y-m-d H:i:s");
                    $user = $_SESSION['s_username'];
                    $tema = $_POST['tema'];
                    $premium = $_SESSION["premium"];

                    $m = new Model(Config::$mvc_bd_nombre, Config::$mvc_bd_usuario,Config::$mvc_bd_clave, Config::$mvc_bd_hostname);

                    $params['galeria'] = $m->verGaleria($id);

                    //Con esto vemos si es premium y sino solo le dejamos subir 5 fotos
                        if(count($params['galeria']) < 5 || $premium === "si") {
                            if($m->subirImagen($id,$nombre,$url,$fecha,$tema) != false) {
                                $m->subirArchivo($nombreFichero, $id);
                                $params['mensaje'] = "Imagen subida correctamente";
                                header('refresh:1; url=index.php?ctl=perfil');
                            } else {
                                $params['mensaje'] = "No se ha podido subir la imagen";
                            }
                        } else {
                            $params['mensaje'] = "No eres usuario premium"; 
                        }
                } else {
                     $params['mensaje'] = "Faltan completar datos";
                }
            }
        }
        require __DIR__ . '/templates/perfil.php';
    }

    public function inicioAdmin() {
        $params = array(
            'username' => '',
            'password' => ''
        );

        $m = new Model(Config::$mvc_bd_nombre, Config::$mvc_bd_usuario,Config::$mvc_bd_clave, Config::$mvc_bd_hostname);

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            // comprobar campos formulario

            if($m->validarUsuario($_POST['useradmin'], $_POST['passadmin'])) {
              $usuario = array();
              $usuario = $m->loguearUsuario($_POST['useradmin'], $_POST['passadmin']);

                if(count($usuario) > 0 && $usuario[0]['username'] == "admin") {
                    $_SESSION["s_username"] = $usuario[0]['username'];
                    $_SESSION["ID"] = $usuario[0]['ID'];
                    $_SESSION["admin"] = "SI";

                    header('Location: index.php?ctl=admin');

                } else {
                    $params['mensaje'] = "Login incorrecto";
                }

            } else {
                $params = array(
                    'username' => '',
                    'password' => ''
                );

                $params['mensaje'] = 'No se ha podido iniciar sesión';
            }
        }
        require __DIR__ . '/../admin/templates/inicio.php';

    }

    public function admin() {
        require __DIR__ . '/../admin/templates/admin.php';
    }

    public function borrarUsers() {
        $m = new Model(Config::$mvc_bd_nombre, Config::$mvc_bd_usuario,Config::$mvc_bd_clave, Config::$mvc_bd_hostname);

        $params = array(
            'usuarios'  => $m->dameUsuarios() 
        );

        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            $id = $_POST['idUser'];
            $carpeta = "imgs/fotos_user_$id";
            $res= $m->borrarUsuario($id);
                if($res != false) {
                    $m -> eliminarDir($carpeta);
                    $params['mensaje'] = "Usuario borrado";
                    header('refresh:1; url=index.php?ctl=borrarUsers');
                } else {
                    $params['mensaje'] = "Usuario no borrado";
                }
        }

        require __DIR__ . '/../admin/templates/borrarUsers.php';
    }

    public function borrarFoto() {
        $m = new Model(Config::$mvc_bd_nombre, Config::$mvc_bd_usuario,Config::$mvc_bd_clave, Config::$mvc_bd_hostname);

        $params = array(
            'imagenes' => $m->dameImagenes() 
        );

        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            $id = $_POST['idImagen'];
            $res= $m->borrarFoto($id);
                if($res != false) {
                    $params['mensaje'] = "Foto borrada";
                    header('refresh:1; url=index.php?ctl=borrarFoto');
                } else {
                    $params['mensaje'] = "Foto no borrada";
                }
        }

        require __DIR__ . '/../admin/templates/borrarFoto.php';
    }

    public function verAviso() {

        require __DIR__ . '/templates/aviso.php';
    }

    public function cerrar() {

        require __DIR__ . '/templates/cerrar.php';
    }

}

?>