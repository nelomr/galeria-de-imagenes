<?php ob_start() ?> 
	<div class="galeria">
		<?php foreach ($params['galeria'] as $galeria) : ?>
			<div class="galeria-box">
				<div class="galeria-box--name"><?php echo $galeria["nombre"] ?></div>
				<div class="galeria-box--img">
					<a href="#" data-featherlight="<?php echo "imgs/".$galeria['url'] ?>">
						<img src="<?php echo "imgs/".$galeria['url'] ?>" alt="<?php echo $galeria['nombre'] ?>" />
					</a>
				</div>
				<div class="galeria-box--comentario">
					<p><a href="index.php?ctl=ver&id=<?php echo $galeria['ID'] ?>">Ver comentarios</a></p>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
<?php $contenido = ob_get_clean() ?> 
	
<?php include 'layout.php' ?>