<!-- 
Autor: Nelo Martinez Rodrigo
Licencia: http://creativecommons.org/licenses/by-nc/4.0/ 
--> 

<html lang="ES">
<head>
<meta charset="utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Galeria multiusuarios</title>
<link href="<?php echo 'css/'.Config::$mvc_css?>" rel="stylesheet" type="text/css" />
<link href="css/featherlight.min.css" type="text/css" rel="stylesheet" />
<link href="css/featherlight.gallery.min.css" type="text/css" rel="stylesheet" />
</head>
<body>
	<div class ="header">
		<div class="header-text"><a href="index.php?ctl=inicio"><h1>ShowYourPhoto - Gallery</h1></a></div>
	<?php
	if(!isset($_SESSION["logeado"])){ 
	  include('menureg.php'); 
	} elseif($_SESSION["logeado"] === "SI") { 
	  include('menuuser.php'); 
	} elseif($_SESSION["admin"] === "SI") { 
	}
	?>
	</div>
	<div class="main">
		<div class="container">
			<?php echo $contenido ?>
		</div>
	</div>
	
	<footer>
		<div class="box-footer">
			<a href="index.php?ctl=aviso" class="legal-text"> Texto legal</a>
			<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">
				<img alt="Licencia de Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" />
			</a>
		</div>
	</footer>

	<script src="js/jquery.js" type="text/javascript" charset="utf-8" ></script>
	<script src="js/featherlight.min.js" type="text/javascript" charset="utf-8" ></script>
	<script src="js/featherlight.gallery.min.js" type="text/javascript" charset="utf-8"></script>
	<script src="js/functions.js" type="text/javascript" charset="utf-8" ></script>

<script>
$(document).ready(function(){
	$('.gallery').featherlightGallery({
	    gallery: {
					fadeIn: 300,
					fadeOut: 300
				},
				openSpeed:    300,
				closeSpeed:   300
		});
});
</script>
</body>
</html>