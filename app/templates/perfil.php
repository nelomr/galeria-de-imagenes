<?php ob_start() ?>
	<?php if(isset($params['mensaje'])) :?>
		<p class="message-information"><span><?php echo $params['mensaje'] ?></span></p> 
	<?php endif; ?> 
	
	<div class="perfil">
    <div class="perfil-box">
      <div class="perfil-name">
        <p>Tu nombre de usuario:<span><?=$_SESSION['s_username']?></span></p>
      </div>
      <div class="perfil-galeria">
        <p>Tu galeria:<span><a href="index.php?ctl=galeria&id=<?php echo $_SESSION['ID'] ?>">Ver Galeria</a></span></p>
      </div>
      <div class="perfil-mail">
        <p>Tu email:<span><?=$_SESSION['email']?></span></p>
      </div>
      <?php if(isset($params['imagenes']) && count($params['imagenes']) > 0) :?>
        <div class="form-delete--img">
          <form class="form" id="form2" method="post" action="index.php?ctl=perfil">
            <label>Nombre de la foto a borrar:</label>
            <select name="idImagen">
            <?php foreach ($params['imagenes'] as $imagen) : ?>
              <option value="<?php echo $imagen['ID'] ?>"><?php echo $imagen["nombre"] ?></option>
            <?php endforeach; ?>
            </select>     
            <input class="btn input-delete-btn" type="submit" name="Submit" value="Borrar" />      
          </form>
        </div>
      <?php endif; ?>
    </div>
    <div class="perfil-box">
      <form class="form" name="form1" method="post" action="index.php?ctl=perfil">
        <label>Contraseña vieja:</label>
          <input name="passwordViejo" type="password" id="passv" value="" />
        <label> Nueva contraseña:</label>
          <input name="password1" type="password" />
        <label>Repetir nueva contraseña:</label>
          <input name="password2" type="password" /> 
        <input class="btn input-btn" type="submit" name="Submit" value="Cambiar contraseña">
  	  </form>
    </div>
    <div class="perfil-box">
      <form class="form" name="form2" method="post" enctype="multipart/form-data" action="index.php?ctl=perfil">
            <label>Nombre de la imagen:</label>
            <input name="nombre" type="text" id="nombre">
            <label>Tema</label>
            <select name="tema">
              <?php foreach ($params['temas'] as $tema) : ?>
                <option value="<?php echo $tema['ID'] ?>"><?php echo $tema['tema'] ?></option>
              <?php endforeach; ?>
            </select>
            <input type="hidden" name="MAX_FILE_SIZE" value="2000000" />
            <input type="file" id="imagen" name="imagen" class="inputfile" />
            <label class="form-input--file" for="imagen">Selecciona tu Imagen</label>
            <input class="btn input-btn" type="submit" name="Submit" value="Subir tu imagen" />
      </form>
    </div>
  </div>
  

<?php $contenido = ob_get_clean() ?> 
	
<?php include 'layout.php' ?>