<?php ob_start() ?> 
<?php if(isset($params['mensaje'])) :?>
	<p class="message-information"><span><?php echo $params['mensaje'] ?></span></p> 
<?php endif; ?>
	<div class="imagen">
		<?php foreach ($params['imagen'] as $imagen) : ?>
			<div class="imagen-box">
				<div class="imagen-box--name"><?php echo $imagen["nombre"] ?></div>
				<div class="imagen-box--img"><img src="<?php echo "imgs/".$imagen['url'] ?>" width="184" height="138" alt="<?php echo $imagen['nombre'] ?>" /></div>
				<?php if(isset($_SESSION["logeado"]) && $_SESSION["logeado"] == "SI"): ?>
				<div class="imagen-box-form">
					<form class="form is-imagen" name="form1" method="post" action="index.php?ctl=ver&id=<?php echo $imagen['ID'] ?>">
		  				<textarea name="comentario" placeholder="comentario..."></textarea>
		  				<input class="btn input-btn" type="submit" name="Submit" value="Enviar comentario">
					</form>
				</div>
				<?php endif; ?>
			</div>
			<?php if(count($params2['comentarios']) > 0) :?>
				<div class="imagen-box is-right">
					<?php foreach ($params2['comentarios'] as $comentario) : ?>
						<div class="imagen-box--comentarios">
							<p class="imagen-box--comentarios-comentario"><?php echo $comentario['texto'] ?>
								<span class="imagen-box--comentarios-author"><?php echo $comentario['username'] ?></span>
							</p>
						</div>	
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		<?php endforeach; ?>
	</div>

<?php $contenido = ob_get_clean() ?> 
	
<?php include 'layout.php' ?>