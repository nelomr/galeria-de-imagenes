<?php ob_start() ?>
	<?php if(!isset($_SESSION["logeado"])): ?>
		<p class="message-information is-upload"><span>Tienes que estar registrado para poder subir imágenes</span></p>
	<?php endif;?>
	<div class="formulario is-center">
		<form class="form-tema" name="form1" method="post" action="index.php?ctl=inicio">
			<label>Buscar por Tema</label>
			<select class="is-center" name="tema">
				<option value="Todas">Todas</option>
				<?php foreach ($params2['temas'] as $tema) : ?>
				<option value="<?php echo $tema['ID'] ?>"><?php echo $tema['tema'] ?></option>
				<?php endforeach; ?>
			</select>
			<input class="btn input-btn" type="submit" name="Submit" value="Buscar por tema">
		</form>
	</div> 
	<?php if (isset($params['imagenes'])):
			if (count($params['imagenes'])>0): ?>
				<div class="galeria">
					<?php foreach ($params['imagenes'] as $imagen) : ?>
					<div class="galeria-box">
						<div class="galeria-box--img">
							<a class="gallery" href="#" data-featherlight="<?php echo "imgs/".$imagen['url'] ?>">
								<img src="<?php echo "imgs/".$imagen['url'] ?>" alt="">
							</a>
						</div>
						<div class="galeria-box--comentario">
							<p><a href="index.php?ctl=ver&id=<?php echo $imagen['ID'] ?>">Ver comentarios</a></p>
						</div>
					</div>
					<?php endforeach; ?>		
				</div>
	 		<?php endif; ?>
	<?php endif; ?>

<?php $contenido = ob_get_clean() ?> 
	
<?php include 'layout.php' ?>