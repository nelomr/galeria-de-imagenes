<?php 

class Model { 
	protected $conexion;

	public function __construct($dbname,$dbuser,$dbpass,$dbhost) { 
		$mvc_bd_conexion = mysql_connect($dbhost, $dbuser, $dbpass);

		if (!$mvc_bd_conexion) { 
			die('No ha sido posible realizar la conexión con la base de datos: ' . mysql_error()); 
		} 

		mysql_select_db($dbname, $mvc_bd_conexion); 
		mysql_set_charset('utf8'); 
		$this->conexion = $mvc_bd_conexion; 
	}

	public function dameImagenes() { 
		$sql = "SELECT * FROM imagenes ORDER BY fecha DESC"; 
		$result = mysql_query($sql, $this->conexion); 
		$imagenes = array(); 
		while ($row = mysql_fetch_assoc($result)) { 
			$imagenes[] = $row; 
		}
		return $imagenes; 
	}

	public function dameTemas() { 
		$sql = "SELECT * FROM temas ORDER BY ID DESC"; 
		$result = mysql_query($sql, $this->conexion); 
		$temas= array(); 
		while ($row = mysql_fetch_assoc($result)) { 
			$temas[] = $row; 
		}
		return $temas;
	}

	public function dameImagenesUser($id) { 
		$sql = "SELECT * FROM imagenes WHERE usId = '$id'"; 
		$result = mysql_query($sql, $this->conexion); 
		$imagenes = array(); 
		while ($row = mysql_fetch_assoc($result)) { 
			$imagenes[] = $row; 
		}
		return $imagenes; 
	}

	public function buscarPorTema($tema) { 
		$sql = "SELECT * FROM imagenes WHERE temaId = '$tema' ORDER BY fecha DESC"; 
		$result = mysql_query($sql, $this->conexion); 
		$imagenes = array(); 
		while ($row = mysql_fetch_assoc($result)) { 
			$imagenes[] = $row; 
		}
		return $imagenes; 
	}

	public function dameUsuarios() { 
		$sql = "SELECT * FROM usuarios WHERE username != 'admin' ORDER BY ID DESC "; 
		$result = mysql_query($sql, $this->conexion); 
		$usuarios = array(); 
		while ($row = mysql_fetch_assoc($result)) { 
			$usuarios[] = $row; 
		}
		return $usuarios; 
	}

	public function dameUsuariosConGaleria() { 
		$sql = "SELECT * FROM usuarios WHERE ID = ANY(SELECT usId FROM imagenes) AND username != 'admin' ORDER BY ID DESC"; 
		$result = mysql_query($sql, $this->conexion); 
		$usuarios = array(); 
		while ($row = mysql_fetch_assoc($result)) { 
			$usuarios[] = $row; 
		}
		return $usuarios; 
	}

	public function dameUsuario($id) { 
		$sql = "SELECT * FROM usuarios WHERE ID = '$id'"; 
		$result = mysql_query($sql, $this->conexion); 
		$usuario = array(); 
		while ($row = mysql_fetch_assoc($result)) { 
			$usuario[] = $row; 
		}
		return $usuario; 
	}

	public function loguearUsuario($user, $pass) { 
		$sql = "SELECT * FROM usuarios WHERE username = '$user' AND password = '$pass'"; 
		$result = mysql_query($sql, $this->conexion); 
	
		$usuario = array(); 
		while ($row = mysql_fetch_assoc($result)) { 
			$usuario[] = $row; 
		}
		return $usuario;  
	}

	public function insertarUsuario($user,$pass,$mail,$premium) { 
		
		$sql2 = "SELECT * FROM usuarios WHERE username = '$user' OR email = '$mail'";
		$result2 = mysql_query($sql2,$this->conexion);

		if(mysql_affected_rows() > 0) {
					return false;
		} else {
			
			$sql = "INSERT INTO usuarios (username,password,premium,email) VALUES ('" .$user . "','" . $pass . "','" . $premium . "','" . $mail . "')";
			$result = mysql_query($sql,$this->conexion);

			return $result;
		}
	}

	public function borrarUsuario($id) { 
		$nombreDirectorio = "imgs/";
		$sql = "DELETE FROM usuarios WHERE ID = '$id'";
		$result = mysql_query($sql,$this->conexion);

		array_map('unlink', glob("$nombreDirectorio.$id.*.jpg"));
		
		return $result;
	}

	public function borrarFoto($id) { 
		$sql = "DELETE FROM imagenes WHERE ID = '$id'";
		$result = mysql_query($sql,$this->conexion);
		
		return $result;
	}

	public function borrarImg($id) { 
		$sql = "SELECT * FROM imagenes WHERE ID = '$id'";
		$result = mysql_query($sql,$this->conexion);
		$res = array();
		while ($row = mysql_fetch_assoc($result)) { 
			$res[] = $row; 
		}
	
		$img="../web/imgs/".$res[0]['url'];
		
		unlink($img); 
	}

	public function cambiarPassword($id, $password) {
		$sql = "UPDATE usuarios SET password='$password' WHERE ID='$id'";
		$result = mysql_query($sql,$this->conexion);

		return $result;
	}

	public function verGaleria($id) { 
		$sql = "SELECT * FROM imagenes WHERE usId='$id' ORDER BY ID DESC"; 
		$result = mysql_query($sql, $this->conexion); 
		$galeria = array(); 
		while ($row = mysql_fetch_assoc($result)) { 
			$galeria[] = $row; 
		}
		return $galeria; 
	}

	public function verImagen($id) { 
		$sql = "SELECT * FROM imagenes WHERE ID='$id' LIMIT 1"; 
		$result = mysql_query($sql, $this->conexion); 
		$imagen = array(); 
		while ($row = mysql_fetch_assoc($result)) { 
			$imagen[] = $row; 
		}
		return $imagen; 
	}

	public function insertarComentario($idUser,$idImagen,$texto) { 
		$sql = "INSERT INTO comentarios (ID, usId,imgId,texto) VALUES (null, '$idUser','$idImagen','$texto')"; 
		$result = mysql_query($sql, $this->conexion); 
		
		return $result; 
	}

	public function dameComentarios($id) { 
		$sql = "SELECT comentarios.*, usuarios.username FROM comentarios, usuarios WHERE imgId = '$id' AND usId = usuarios.ID ORDER BY ID DESC"; 
		$result = mysql_query($sql, $this->conexion); 
		$comentarios = array(); 
		while ($row = mysql_fetch_assoc($result)) { 
			$comentarios[] = $row; 
		}
		return $comentarios; 
	}

	public function subirImagen($id,$nombre,$url,$fecha,$tema) { 
		$sql = "INSERT INTO imagenes (ID, usId,nombre,url,fecha,temaId) VALUES (null, '$id','$nombre','$url','$fecha','$tema')"; 
		$result = mysql_query($sql, $this->conexion); 
		
		return $result; 
	}

	public function subirArchivo($nombreFichero,$id){
		if(is_uploaded_file($_FILES['imagen']['tmp_name'])){
			$extension_archivo = $_FILES['imagen']['type'];//vemos la extension del archivo
			if($extension_archivo!='image/jpeg' && $extension_archivo!='image/gif' && $extension_archivo!='image/jpg' ){//Comprobamos la extension
				echo("El tipo de archivo no está permitido<br/>");
			}else{
				$nombreDirectorio = "imgs/fotos_user_$id/";

				if (!file_exists($nombreDirectorio)) {
    				mkdir($nombreDirectorio, 0777, true);
				}				

			$nombreCompleto = $nombreDirectorio.$nombreFichero;
												
				if(move_uploaded_file($_FILES['imagen']['tmp_name'],$nombreDirectorio.$nombreFichero)){//si todo es correcto subimos el archivo
				}else{
					echo ("No se ha podido subir la foto.<br/>");
				}	
			}							
	  	}
	}

	public function eliminarDir($carpeta) {
		 $path = rtrim( strval( $carpeta ), '/' ) ;
    
		    $d = @dir( $path );
		    
		    if( ! $d )
		        return false;
		    
		    while ( false !== ($current = $d->read()) )
		    {
		        if( $current === '.' || $current === '..')
		            continue;
		        
		        $file = $d->path . '/' . $current;
		        
		        if( @is_dir($file) )
		            removeDirectory($file);
		        
		        if( is_file($file) )
		            unlink($file);
		    }
		    
		    @rmdir( $d->path );
		    $d->close();
		    return true;
	}

	public function validarDatos($user,$pass,$pass2){

		return(htmlentities($user) & htmlentities($pass) & htmlentities($pass2));
	}

	public function validarUsuario($user,$pass){

		return(htmlentities($user) & htmlentities($pass));
	}

	
}
?>