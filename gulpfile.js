var gulp = require('gulp');
var sass = require('gulp-sass');
var livereload = require('gulp-livereload');

gulp.task('styles', function() {
    gulp.src('web/sass/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('web/css/'))
        .pipe(livereload());
});

//Watch task
gulp.task('default',function() {
	livereload.listen();
    gulp.watch('web/sass/**/*.scss',['styles']);
});