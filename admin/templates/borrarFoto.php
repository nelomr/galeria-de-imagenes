<?php ob_start() ?>
<?php
if($_SESSION["admin"] != "SI"){ 
header('Location: index.php?ctl=inicio');
}
?>

<div class="admin-welcome">
    <?php if(isset($params['mensaje'])) :?>
        <p class="message-information"><span><?php echo $params['mensaje'] ?></span></p> 
    <?php endif; ?>
    <h3>Sistema de administracion</h3>
    <p>Borrado de fotos.</p>
    <div class="formulario is-center">   
      <form class="form admin-form" method="post" action="">
        <label>Nombre de la foto a borrar:</label>
        <select name="idImagen">
        <?php foreach ($params['imagenes'] as $imagen) : ?>
            <option value="<?php echo $imagen['ID'] ?>"><?php echo $imagen["url"] ?></option>
        <?php endforeach; ?>
        </select>     
        <input class="btn input-delete-btn" type="submit" name="Submit" value="Borrar foto" />      
      </form>
<?php $contenido = ob_get_clean() ?> 
  
<?php include 'layout.php' ?>