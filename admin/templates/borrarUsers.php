<?php ob_start() ?>
<?php
if($_SESSION["admin"] != "SI"){ 
header('Location: index.php?ctl=inicio');
}
?>

<div class="admin-welcome">
    <?php if(isset($params['mensaje'])) :?>
        <p class="message-information"><span><?php echo $params['mensaje'] ?></span></p> 
    <?php endif; ?>
    <h3>Sistema de administracion</h3>
    <p>Borrado de usuarios.</p>
    <div class="formulario is-center">   	
        <form class="form" method="post" action="">
            <label>Nombre del usuario a borrar:</label>
            <select name="idUser">
            <?php foreach ($params['usuarios'] as $usuario) : ?>
                <option value="<?php echo $usuario['ID'] ?>"><?php echo $usuario["username"] ?></option>
            <?php endforeach; ?>
            </select>     
            <input class="btn input-delete-btn" type="submit" name="Submit" value="Borrar usuario" />      
        </form>
    </div>
</div>
<?php $contenido = ob_get_clean() ?> 
  
<?php include 'layout.php' ?>