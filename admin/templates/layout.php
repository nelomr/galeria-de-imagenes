<html lang="ES">
<head>
<meta charset="utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Galeria multiusuarios</title>
<link href="<?php echo '../web/css/'.Config::$mvc_css?>" rel="stylesheet" type="text/css" />
<link href="../web/css/featherlight.min.css" type="text/css" rel="stylesheet" />
<link href="../web/css/featherlight.gallery.min.css" type="text/css" rel="stylesheet" />
</head>
<body>
	<div class ="header is-admin">
		<div class="header-text">
			<a href="index.php?ctl=admin">
				<h1>Panel Admin</h1>
			</a>
		</div>
	</div>	
	<div class="main-admin">
		<?php if(isset($_SESSION["admin"]) && $_SESSION["admin"] == "SI"){?>
	
		    <div class="menu-admin">
		    	<a href="index.php?ctl=borrarUsers">Borrar usuarios</a> 
		    	<a href="index.php?ctl=borrarFoto">Borrar fotos</a>
		    	<a href="index.php?ctl=cerrar">Cerrar sesion </a>
		    </div>
  	
 		<?php }?>
		<?php echo $contenido ?>
	</div>

	<script src="../web/js/jquery.js" type="text/javascript" charset="utf-8" ></script>
	<script src="../web/js/featherlight.min.js" type="text/javascript" charset="utf-8" ></script>
	<script src="../web/js/featherlight.gallery.min.js" type="text/javascript" charset="utf-8"></script>
</body>
</html>