﻿<?php
session_start();
//carga el modelo y los controladores

require_once __DIR__ . '/../app/config.php';
require_once __DIR__ . '/../app/Model.php';
require_once __DIR__ . '/../app/Controllers.php';

//enrutamiento

$map = array (
                'inicio' => array('controller' => 'Controller', 'action' =>'inicioAdmin'),
                'admin' => array('controller' => 'Controller', 'action' =>'admin'),
                'borrarUsers' => array('controller' => 'Controller', 'action' =>'borrarUsers'),
                'borrarFoto' => array('controller' => 'Controller', 'action' =>'borrarFoto'),
                'cerrar' => array('controller' => 'Controller', 'action' =>'cerrar')
    );

// Parseo de la ruta

if (isset($_GET['ctl'])) {
    if (isset($map[$_GET['ctl']])) {
        $ruta = $_GET['ctl'];
    } else {
        header('Status: 404 Not Found');
        echo '<html>
                <body>
                    <p>Error 404: No existe la ruta <i>' .$_GET['ctl'] .'</i></p>
                </body>
            </html>';
        exit;
    }
} else {
    $ruta = 'inicio';
}

$controlador = $map[$ruta];

if (method_exists($controlador['controller'], $controlador['action'])) {
    call_user_func(array(new $controlador['controller'], $controlador['action']));
} else {
    header('Status: 404 Not Found');
    echo '<html>
                <body>
                    <p>Error 404: El controlador <i>' . $controlador['controller'] .' -> '. $controlador['action'] .'</i></p>
                </body>
            </html>';
}

?>